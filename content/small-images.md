---
title: Small images
id: small-images
layout: two-column
next: demo5
---
 * a significant fraction of the CI time might be spent pulling the image
 * a smaller image is generally better
 * but you can't delete stuff
 * could just make a very large `RUN` command but that's slow to build
 * Podman / Docker caches build stages
 * solution: multi-stage builds
---
```docker
FROM alpine:3.11 AS builder
RUN apk add --no-cache gcc musl-dev
WORKDIR /my-project/
COPY test.c /my-project/
RUN gcc -o test test.c
RUN apk del gcc musl-dev

CMD /my-project/test

FROM alpine:3.11
COPY --from=builder / /
```
