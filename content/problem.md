---
title: the problem
id: problem
start: true
next: other-problem
time: 120
---
<div>

 * all code has dependencies (at least compiler & environment)
```
$ ./test
./test: error while loading shared libraries: libgolem.so.0:
    cannot open shared object file: No such file or directory
```
 * CI systems need those (cf. [Hitham's talk](https://github.com/Hitham2496/Subtractor))
 * a lot of problems are ultimately due to that
 * disk space is free, time isn't
 * solution: just bundle *all* dependencies (and a tiny linux
   installation) with the code
 * essentially `chroot` on steroids

</div>
