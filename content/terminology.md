---
title: terminology
id: terminology
next: setup
time: 110
---
 * *image*: a **static** snapshot of the 'hard disk' of your system
   (think `tar cf image.tar /`)
 * *container*: an **ephemeral** instance of the image (think virtual
   computer)
 * *VM* or *virtual machine*: something a container isn't
 * *registry*: a place to store images,
   [DockerHub](https://hub.docker.com/) being the biggest (think
   GitLab)
 * *OCI Specifcations*: the standard that defines these things (think
   C++11)
 * [*Docker*](https://docker.com),
   [*podman*](https://podman.io/),
   [*Buildah*](https://buildah.io/),
   [*udocker*](https://github.com/indigo-dc/udocker):
   implementation of (parts of) this standard (think g++)
