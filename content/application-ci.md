---
title: Application -- CI
id: application-ci
layout: two-column
next: small-images
---

 * CI jobs need to run somewhere
 * usually done in a container that has all the dependencies to carry
   out the job
 * [for this repo](https://gitlab.com/yannickulrich/docker-talk/-/blob/root/.gitlab-ci.yml)
```yaml
pages:
  stage: deploy
  image: registry.gitlab.com/transparencies/transparencies:latest
  script:
    - transparencies head.yaml
  artifacts:
  paths:
    - public/
```
---
<style type="text/css">
.mycode {
  height: 100%;
  width: 100%;
  display: flex;
}
.term-fg-green {
  color: #090;
}
.job-log {
  width: 100%;
  height: 100%;
  font-family: "Menlo", "DejaVu Sans Mono", "Liberation Mono", "Consolas", "Ubuntu Mono", "Courier New", "andale mono", "lucida console", monospace;
  padding: 8px 12px;
  margin: 0 0 8px;
  font-size: 15px;
  word-break: break-all;
  word-wrap: break-word;
  color: #fff;
  border-radius: 2px;
  min-height: 42px;
  background-color: #111;
}
.line-number {
  color: #666;
  padding: 0 8px;
  padding-right: 8px;
  min-width: 50px;
  padding-right: 1em;
  -webkit-user-select: none;
  user-select: none;
}
.term-fg-l-cyan {
  color: #00bdbd;
}
.term-bold {
    font-weight: 600;
}
.term-fg-l-green {
  color: #00d600;
}
</style>
<div class="mycode">
<code data-qa-selector="job_log_content" class="job-log d-block" job-log="[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object],[object Object]" is-complete="true"><div class="js-line log-line"><a id="L1" href="/yannickulrich/docker-talk/-/jobs/2228457092#L1" class="gl-link d-inline-block text-right line-number flex-shrink-0">1</a><span class="gl-white-space-pre-wrap">Running with gitlab-runner 14.8.0~beta.44.g57df0d52 (57df0d52)</span></div><div class="js-line log-line"><a id="L2" href="/yannickulrich/docker-talk/-/jobs/2228457092#L2" class="gl-link d-inline-block text-right line-number flex-shrink-0">2</a><span class="gl-white-space-pre-wrap">&nbsp;&nbsp;on blue-4.shared.runners-manager.gitlab.com/default J2nyww-s</span></div><div><div role="button" class="log-line collapsible-line d-flex justify-content-between ws-normal gl-align-items-flex-start"> <a id="L3" href="/yannickulrich/docker-talk/-/jobs/2228457092#L3" class="gl-link d-inline-block text-right line-number flex-shrink-0">3</a><span class="line-text w-100 gl-white-space-pre-wrap term-fg-l-cyan term-bold">Preparing the "docker+machine" executor</span> <span class="badge badge-muted badge-pill gl-badge md"></span></div> <div class="js-line log-line"><a id="L4" href="/yannickulrich/docker-talk/-/jobs/2228457092#L4" class="gl-link d-inline-block text-right line-number flex-shrink-0">4</a><span class="gl-white-space-pre-wrap">Using Docker executor with image registry.gitlab.com/transparencies/transparencies:latest ...</span></div><div class="js-line log-line"><a id="L5" href="/yannickulrich/docker-talk/-/jobs/2228457092#L5" class="gl-link d-inline-block text-right line-number flex-shrink-0">5</a><span class="gl-white-space-pre-wrap">Authenticating with credentials from job payload (GitLab Registry)</span></div><div class="js-line log-line"><a id="L6" href="/yannickulrich/docker-talk/-/jobs/2228457092#L6" class="gl-link d-inline-block text-right line-number flex-shrink-0">6</a><span class="gl-white-space-pre-wrap">Pulling docker image registry.gitlab.com/transparencies/transparencies:latest ...</span></div><div class="js-line log-line"><a id="L7" href="/yannickulrich/docker-talk/-/jobs/2228457092#L7" class="gl-link d-inline-block text-right line-number flex-shrink-0">7</a><span class="gl-white-space-pre-wrap">Using docker image sha256:690b1fcec27baf540bbc79b4b0b22f22a49bc280cb97c1c93a1f8633c71b543e for registry.gitlab.com/transparencies/transparencies:latest with digest registry.gitlab.com/transparencies/transparencies@sha256:41a6e534778f2bb24a746f5d5611a4fcc31356cda23f7906769adab934559f4b ...</span></div></div><div><div role="button" class="log-line collapsible-line d-flex justify-content-between ws-normal gl-align-items-flex-start"> <a id="L9" href="/yannickulrich/docker-talk/-/jobs/2228457092#L9" class="gl-link d-inline-block text-right line-number flex-shrink-0">9</a> <span class="line-text w-100 gl-white-space-pre-wrap term-fg-l-cyan term-bold">Preparing environment</span> <span class="badge badge-muted badge-pill gl-badge md"></span></div> <div class="js-line log-line"><a id="L10" href="/yannickulrich/docker-talk/-/jobs/2228457092#L10" class="gl-link d-inline-block text-right line-number flex-shrink-0">10</a><span class="gl-white-space-pre-wrap">Running on runner-j2nyww-s-project-34283321-concurrent-0 via runner-j2nyww-s-shared-1647862815-65517755...</span></div></div> <!----></code>
</div>
</div>
