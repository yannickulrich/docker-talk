---
title: Technical background
id: technical
next: interaction
---
 * OCI uses [OverlayFS](https://www.kernel.org/doc/html/latest/filesystems/overlayfs.html?highlight=overlayfs)
 * each layer is immutable and addressable (like git commits)
 * changes are done in a new layer
 * abstracted away by the kernel (3.18 or later)
 * **important**: you can't actually delete things! once added, it's added

```
$ podman history my-project:latest
ID            CREATED         CREATED BY                                     SIZE
dbb12fff0786  13 seconds ago  /bin/sh -c #(nop) CMD /my-project/test         0 B
<missing>     13 seconds ago  /bin/sh -c gcc -o test test.c                  24.6 kB
533288ac28c7  14 seconds ago  /bin/sh -c #(nop) COPY file:4500777be0cc6a...  2.56 kB
ebdceb13b347  14 seconds ago  /bin/sh -c #(nop) WORKDIR /my-project/         0 B
<missing>     16 seconds ago  /bin/sh -c apk update && apk add gcc musl-dev  111 MB
a787cb986503  4 months ago    /bin/sh -c #(nop)  CMD ["/bin/sh"]             0 B
<missing>     4 months ago    /bin/sh -c #(nop) ADD file:efe2d94a88cdbbd...  5.89 MB
```
