---
title: Volumes
id: volumes
next: application-mcmule
---
 * this is clearly not practical, how do we get results from our
   container

 * *volumes*: mount a host folder into the container
 * slight complication: SELinux
 * host folder should exist
```
$ podman run --rm -it -v /path/to/host/folder:/path/to/container/folder:z <container name>
```
 * becomes accessible from container, just dump output into
   `/path/to/container/folder`
