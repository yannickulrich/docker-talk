---
title: the other problem
id: other-problem
next: terminology
time: 105
---
 * Let's say I want to try something with an old version of
   [Professor](https://professor.hepforge.org/)
   * ... which depends on [Rivet](https://rivet.hepforge.org/)
      * ... which depends on [root](https://root.cern.ch)
        * ... which ultimately depends on gcc
      * ... which depends on [TeXLive](https://tug.org/texlive/)
        * ... which depends on perl
   * ... which depends on [matplotlib](https://matplotlib.org/)
      * ... which depends on python
        * ... which depends on gcc

 * all of these might depend on versions you don't have on your system
 * recipe for disaster if you tried it anyway
 * all just for a quick experiment
