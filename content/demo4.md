---
title: Demo 4
id: demo4
next: application-ci
---

<style>
.ascii {
    height: 100%;
    width: 80%;
    border: none;
}
</style>
<iframe class="ascii" src="https://asciinema.org/a/cVKxHrS4EEvRT9peDNBaSgrQR/iframe"></iframe>
