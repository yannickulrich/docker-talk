---
title: interacting with containers
id: interaction
next: demo3
---
 * the container is isolated from the main OS *and* ephemeral
 * how to interact?
   * by hand (`docker run -it <image name> sh` / `docker exec -it <container name> <cmd>`)
   * copying (`docker cp <src> <dest>`)
   * through volumes
   * (through networks, open port in the container)
