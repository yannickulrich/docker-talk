---
title: setup
id: setup
layout: two-column
next: demo1pre
time: 90
---
# Docker
 * pushed by Docker Inc. (duh..)
 * de-facto standard
 * daemon running as root
 * will never work on HPC machines
 * neat for orchestration

## Install
* Fedora: `sudo dnf install docker`
* Ubuntu: [bit more complicated](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)
* macOS: [good luck!](https://docs.docker.com/desktop/mac/install/)


---
# podman
 * pushed by RedHat
 * allegedly a drop-in replacement for `docker`
 * service running as user
 * might work on HPC machines in the future
 * used to be pre-installed on Fedora

## Install
* Fedora: `sudo dnf install podman`
* Ubuntu: `sudo apt install podman`
* macOS: `sudo brew install podman`

