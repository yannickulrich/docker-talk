---
title: Application -- McMule
id: application-mcmule
next: demo4
---
 * McMule is hosted in [GitLab registry](https://gitlab.com/mule-tools/mcmule/container_registry/)
 * floating point arithmetic is reproducible across systems
 * pull as `podman pull registry.gitlab.com/mule-tools/mcmule`
 * demo: CI test & short run
