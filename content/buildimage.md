---
title: building an image
layout: two-column
id: buildimage
next: demo2
time: 120
---
 * many pre-build images available (
    [`gcc:7`](https://hub.docker.com/layers/gcc/library/gcc/7/images/sha256-15e46c7644d08706af72b95b80e57cacfaac12ea163e980581d5bd4fdedf82e6?context=explore),
    [`gcc:10`](https://hub.docker.com/layers/gcc/library/gcc/10/images/sha256-c3c2cc23d4e70d10628ec593018f8bd305c7793efe466c9933752cde2d8add3f?context=explore),
    [`python:3.9`](https://hub.docker.com/layers/python/library/python/3.9/images/sha256-f02ca062c296a4dfa35aed780817689f5c2cda469e1b349968713cf272a1b3a0?context=explore),
    [`pandoc/latex:2.16`](https://hub.docker.com/layers/pandoc/latex/2.16/images/sha256-d9f0a688b9ee31d411ccb08f2b3d6e5aafc7e1d788ae6a01353a2e831bc3f69a?context=explore))

 * often need to make our own though
 * need a `Dockerfile` or `Containerfile`
 * `docker build -t my-project .`

---
```docker
FROM alpine:3.11
RUN apk add --no-cache gcc musl-dev
WORKDIR /my-project/
COPY test.c /my-project/
RUN gcc -o test test.c
CMD /my-project/test
```
